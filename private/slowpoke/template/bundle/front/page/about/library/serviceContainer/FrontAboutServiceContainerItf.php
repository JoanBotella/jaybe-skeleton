<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\page\about\library\serviceContainer;

use slowpoke\framework\library\controller\ControllerServiceItf;
use slowpoke\framework\library\requestMatcher\RequestMatcherServiceItf;
use slowpoke\template\bundle\front\page\about\service\responseBuilder\FrontAboutResponseBuilderServiceItf;
use slowpoke\template\bundle\front\page\about\service\widget\mainContent\FrontAboutMainContentWidgetServiceItf;

interface FrontAboutServiceContainerItf
{

	public function getFrontAboutRequestMatcher():RequestMatcherServiceItf;

	public function getFrontAboutController():ControllerServiceItf;

	public function getFrontAboutResponseBuilder():FrontAboutResponseBuilderServiceItf;

	public function getFrontAboutMainContentWidget():FrontAboutMainContentWidgetServiceItf;

}