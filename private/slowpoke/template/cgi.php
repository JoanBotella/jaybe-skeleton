<?php
declare(strict_types=1);

require __DIR__.'/../../composer/vendor/autoload.php';

use slowpoke\template\library\serviceContainer\ServiceContainer;
use slowpoke\framework\service\cgiAppRunner\CgiAppRunnerInput;

$serviceContainer = new ServiceContainer();

$cgiAppRunner = $serviceContainer->getCgiAppRunner();

$cgiAppRunnerInput = new CgiAppRunnerInput();

$cgiAppRunnerOutput = $cgiAppRunner->run($cgiAppRunnerInput);

if ($cgiAppRunnerOutput->hasErrors())
{
	throw new Exception(
		$cgiAppRunnerOutput->getErrorsAfterHas()[0]->getMessage()
	);
}
