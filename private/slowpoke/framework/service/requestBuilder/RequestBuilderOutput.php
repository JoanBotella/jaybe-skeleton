<?php
declare(strict_types=1);

namespace slowpoke\framework\service\requestBuilder;

use slowpoke\core\library\serviceOutput\ServiceOutputAbs;
use slowpoke\framework\library\request\Request;

final class RequestBuilderOutput extends ServiceOutputAbs
{

	private Request $request;

	public function hasRequest():bool
	{
		return isset(
			$this->request
		);
	}

	public function getRequestAfterHas():Request
	{
		return $this->request;
	}

	public function setRequest(Request $v):void
	{
		$this->request = $v;
	}

	public function unsetRequest():void
	{
		unset(
			$this->request
		);
	}

}