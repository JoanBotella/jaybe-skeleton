<?php
declare(strict_types=1);

namespace slowpoke\template\library\serviceContainer;

use slowpoke\core\library\serviceContainer\CoreServiceContainerItf;
use slowpoke\framework\library\serviceContainer\FrameworkServiceContainerItf;
use slowpoke\template\bundle\front\library\serviceContainer\FrontServiceContainerItf;

interface ServiceContainerItf
extends
	CoreServiceContainerItf,
	FrameworkServiceContainerItf,
	FrontServiceContainerItf
{
}
