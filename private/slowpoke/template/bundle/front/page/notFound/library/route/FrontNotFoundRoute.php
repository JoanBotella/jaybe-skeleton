<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\page\notFound\library\route;

use slowpoke\template\library\route\RouteAbs;
use slowpoke\framework\library\requestMatcher\RequestMatcherServiceItf;
use slowpoke\framework\library\controller\ControllerServiceItf;

final class FrontNotFoundRoute extends RouteAbs
{

	public function getRequestMatcher():RequestMatcherServiceItf
	{
		return $this->serviceContainer->getFrontNotFoundRequestMatcher();
	}

	public function getController():ControllerServiceItf
	{
		return $this->serviceContainer->getFrontNotFoundController();
	}

}