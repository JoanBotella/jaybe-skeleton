
	var route = function()
	{
		var pageElement = document.querySelector('[data-page]');

		if (pageElement === null)
		{
			return;
		}

		var pageCode = pageElement.getAttribute('data-page');

		if (typeof pageSetuppers[pageCode] === 'function')
		{
			pageSetuppers[pageCode]();
			return;
		}

		console.error('The page code "' + pageCode + '" is not valid.');
	}
