<?php
declare(strict_types=1);

namespace slowpoke\framework\test;

use slowpoke\framework\HelloWorld;
use PHPUnit\Framework\TestCase;

final class HelloWorldTest extends TestCase
{

	public function test_greet_returnsHelloWorld():void
	{
		$helloWorld = new HelloWorld();
		$this->assertEquals(
			'Hello World',
			$helloWorld->greet()
		);
	}

}
