<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\page\notFound\service\controller;

use slowpoke\framework\library\controller\ControllerServiceAbs;
use slowpoke\template\bundle\front\page\notFound\service\responseBuilder\FrontNotFoundResponseBuilderServiceItf;
use slowpoke\template\bundle\front\page\notFound\service\responseBuilder\FrontNotFoundResponseBuilderInput;
use slowpoke\core\library\Error;

final class FrontNotFoundControllerService extends ControllerServiceAbs
{
	const ERROR_CODE_NO_RESPONSE_BUILDER_RESPONSE = 0;

	private FrontNotFoundResponseBuilderServiceItf $responseBuilder;

	public function __construct(
		FrontNotFoundResponseBuilderServiceItf $responseBuilder
	)
	{
		$this->responseBuilder = $responseBuilder;
	}

	protected function setupResponse():void
	{
		$responseBuilderInput = new FrontNotFoundResponseBuilderInput();

		$responseBuilderOutput = $this->responseBuilder->run(
			$responseBuilderInput
		);

		if ($responseBuilderOutput->hasErrors())
		{
			$this->output->addErrors(
				$responseBuilderOutput->getErrorsAfterHas()
			);
			return;
		}

		if ($responseBuilderOutput->hasResponse())
		{
			$this->response = $responseBuilderOutput->getResponseAfterHas();
			return;
		}

		$this->output->addError(
			new Error(
				self::ERROR_CODE_NO_RESPONSE_BUILDER_RESPONSE,
				'ResponseBuilder had no errors but had no response'
			)
		);
	}

}