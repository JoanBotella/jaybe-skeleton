<?php
declare(strict_types=1);

use slowpoke\template\bundle\front\page\notFound\service\widget\mainContent\FrontNotFoundMainContentWidgetInput;

/**
 * @var FrontNotFoundMainContentWidgetInput $input
 */

?>

<h1>Not Found</h1>
