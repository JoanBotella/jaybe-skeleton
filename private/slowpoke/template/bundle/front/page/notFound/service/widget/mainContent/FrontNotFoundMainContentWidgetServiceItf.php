<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\page\notFound\service\widget\mainContent;

use slowpoke\framework\library\widget\WidgetOutput;
use slowpoke\template\bundle\front\page\notFound\service\widget\mainContent\FrontNotFoundMainContentWidgetInput;

interface FrontNotFoundMainContentWidgetServiceItf
{

	public function run(FrontNotFoundMainContentWidgetInput $input):WidgetOutput;

}