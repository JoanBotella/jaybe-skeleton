<?php
declare(strict_types=1);

namespace slowpoke\framework\service\responseDispatcher;

use slowpoke\framework\library\response\Response;

final class ResponseDispatcherInput
{

	private Response $response;

	public function __construct(
		Response $response
	)
	{
		$this->response = $response;
	}

	public function getResponse():Response
	{
		return $this->response;
	}

}