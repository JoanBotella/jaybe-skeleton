<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\page\home\service\requestMatcher;

use slowpoke\framework\library\requestMatcher\RequestMatcherServiceAbs;

final class FrontHomeRequestMatcherService extends RequestMatcherServiceAbs
{

	protected function setupIsMatch():void
	{
		$this->isMatch = $this->isRequestPathEqualTo('');
	}

}