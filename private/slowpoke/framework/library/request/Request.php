<?php
declare(strict_types=1);

namespace slowpoke\framework\library\request;

final class Request
{

	private string $path;

	public function __construct(
		string $path
	)
	{
		$this->path = $path;
	}

	public function getPath():string
	{
		return $this->path;
	}

}