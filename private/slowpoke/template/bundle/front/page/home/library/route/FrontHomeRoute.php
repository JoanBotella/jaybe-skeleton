<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\page\home\library\route;

use slowpoke\template\library\route\RouteAbs;
use slowpoke\framework\library\requestMatcher\RequestMatcherServiceItf;
use slowpoke\framework\library\controller\ControllerServiceItf;

final class FrontHomeRoute extends RouteAbs
{

	public function getRequestMatcher():RequestMatcherServiceItf
	{
		return $this->serviceContainer->getFrontHomeRequestMatcher();
	}

	public function getController():ControllerServiceItf
	{
		return $this->serviceContainer->getFrontHomeController();
	}

}