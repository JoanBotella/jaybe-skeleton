<?php
declare(strict_types=1);

namespace slowpoke\framework\service\responseDispatcher;

use slowpoke\framework\service\responseDispatcher\ResponseDispatcherServiceItf;
use slowpoke\framework\service\responseDispatcher\ResponseDispatcherInput;
use slowpoke\framework\service\responseDispatcher\ResponseDispatcherOutput;
use slowpoke\core\library\Error;

final class ResponseDispatcherService implements ResponseDispatcherServiceItf
{
	const ERROR_CODE_NOT_ON_WEB_SERVER = 0;
	const ERROR_CODE_NO_HTTP_STATUS = 1;

	private ResponseDispatcherInput $input;

	private ResponseDispatcherOutput $output;

	public function run(ResponseDispatcherInput $input):ResponseDispatcherOutput
	{
		$this->input = $input;

		$this->setupOutput();

		$output = $this->output;

		$this->tearDown();

		return $output;
	}

		private function setupOutput():void
		{
			$this->output = $this->buildOutput();

			$this->dispatchHttpStatus();

			if ($this->output->hasErrors())
			{
				return;
			}

			$this->dispatchBody();
		}

			private function buildOutput():ResponseDispatcherOutput
			{
				return new ResponseDispatcherOutput();
			}

			private function dispatchHttpStatus():void
			{
				$response = $this->input->getResponse();

				$result = http_response_code(
					$response->getHttpStatus()
				);

				if ($result === true)
				{
					$this->output->addError(
						new Error(
							self::ERROR_CODE_NOT_ON_WEB_SERVER,
							'We are not on a web server environment.'
						)
					);
					return;
				}

				if ($result === false)
				{
					$this->output->addError(
						new Error(
							self::ERROR_CODE_NO_HTTP_STATUS,
							'Http status is not provided and we are not on a web server environment.'
						)
					);
					return;
				}
			}

			private function dispatchBody():void
			{
				$response = $this->input->getResponse();

				echo $response->getBody();
			}

		private function tearDown():void
		{
			unset($this->input);
			unset($this->output);
		}

}