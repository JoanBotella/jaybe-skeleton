<?php
declare(strict_types=1);

namespace slowpoke\core\test\unitary\service\stringReplacer;

use slowpoke\core\service\stringReplacer\StringReplacerInput;
use slowpoke\core\service\stringReplacer\StringReplacerOutput;

final class InputOutputPair
{

	private StringReplacerInput $input;

	private StringReplacerOutput $output;

	public function __construct(
		StringReplacerInput $input,
		StringReplacerOutput $output
	)
	{
		$this->input = $input;
		$this->output = $output;
	}

	public function getInput():StringReplacerInput
	{
		return $this->input;
	}

	public function getOutput():StringReplacerOutput
	{
		return $this->output;
	}

}