<?php
declare(strict_types=1);

namespace slowpoke\framework\service\requestBuilder;

use slowpoke\framework\service\requestBuilder\RequestBuilderOutput;
use slowpoke\framework\service\requestBuilder\RequestBuilderInput;

interface RequestBuilderServiceItf
{

	public function run(RequestBuilderInput $input):RequestBuilderOutput;

}