<?php
declare(strict_types=1);

namespace slowpoke\framework\service\app;

use slowpoke\core\library\serviceOutput\ServiceOutputAbs;
use slowpoke\framework\library\response\Response;

final class AppOutput extends ServiceOutputAbs
{

	private Response $response;

	public function hasResponse():bool
	{
		return isset(
			$this->response
		);
	}

	public function getResponseAfterHas():Response
	{
		return $this->response;
	}

	public function setResponse(Response $v):void
	{
		$this->response = $v;
	}

	public function unsetResponse():void
	{
		unset(
			$this->response
		);
	}

}