<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\page\about\service\widget\mainContent;

use slowpoke\framework\library\widget\WidgetOutput;
use slowpoke\template\bundle\front\page\about\service\widget\mainContent\FrontAboutMainContentWidgetInput;

interface FrontAboutMainContentWidgetServiceItf
{

	public function run(FrontAboutMainContentWidgetInput $input):WidgetOutput;

}