<?php
declare(strict_types=1);

namespace slowpoke\template;

final class HelloWorld
{

	public function greet():string
	{
		return 'Hello World';
	}

}