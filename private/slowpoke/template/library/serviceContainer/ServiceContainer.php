<?php
declare(strict_types=1);

namespace slowpoke\template\library\serviceContainer;

use slowpoke\template\library\serviceContainer\ServiceContainerItf;

use slowpoke\core\library\serviceContainer\CoreServiceContainerTrait;
use slowpoke\framework\library\serviceContainer\FrameworkServiceContainerTrait;
use slowpoke\template\bundle\front\library\serviceContainer\FrontServiceContainerTrait;

use slowpoke\template\bundle\front\page\about\library\route\FrontAboutRoute;
use slowpoke\template\bundle\front\page\home\library\route\FrontHomeRoute;
use slowpoke\template\bundle\front\page\notFound\library\route\FrontNotFoundRoute;

final class ServiceContainer implements ServiceContainerItf
{
	use
		CoreServiceContainerTrait,
		FrameworkServiceContainerTrait,
		FrontServiceContainerTrait
	;

	protected function buildRoutes():array
	{
		return [
			new FrontAboutRoute($this),
			new FrontHomeRoute($this),
			new FrontNotFoundRoute($this),
		];
	}

	protected function getAppBasePath():string
	{
		return '/slowpoke-template/repo/';
	}

}
