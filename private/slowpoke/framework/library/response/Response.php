<?php
declare(strict_types=1);

namespace slowpoke\framework\library\response;

final class Response
{

	private int $httpStatus;

	private string $body;

	public function __construct(
		int $httpStatus,
		string $body
	)
	{
		$this->httpStatus = $httpStatus;
		$this->body = $body;
	}

	public function getHttpStatus():int
	{
		return $this->httpStatus;
	}

	public function getBody():string
	{
		return $this->body;
	}

}