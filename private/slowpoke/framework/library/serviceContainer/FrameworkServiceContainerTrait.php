<?php
declare(strict_types=1);

namespace slowpoke\framework\library\serviceContainer;

use slowpoke\framework\service\app\AppService;
use slowpoke\framework\service\app\AppServiceItf;
use slowpoke\framework\service\router\RouterService;
use slowpoke\framework\service\router\RouterServiceItf;
use slowpoke\framework\service\requestBuilder\RequestBuilderServiceItf;
use slowpoke\framework\service\requestBuilder\RequestBuilderService;
use slowpoke\framework\service\responseDispatcher\ResponseDispatcherServiceItf;
use slowpoke\framework\service\responseDispatcher\ResponseDispatcherService;
use slowpoke\framework\service\cgiAppRunner\CgiAppRunnerServiceItf;
use slowpoke\framework\service\cgiAppRunner\CgiAppRunnerService;
use slowpoke\framework\service\widget\html\HtmlWidgetServiceItf;
use slowpoke\framework\service\widget\html\HtmlWidgetService;

trait FrameworkServiceContainerTrait
{

	private AppServiceItf $app;

	public function getApp():AppServiceItf
	{
		if (!isset($this->app))
		{
			$this->app = $this->buildApp();
		}
		return $this->app;
	}

		protected function buildApp():AppServiceItf
		{
			return new AppService(
				$this->getRouter()
			);
		}

// -----------------------------------------------------------------------------

	private RouterServiceItf $router;

	public function getRouter():RouterServiceItf
	{
		if (!isset($this->router))
		{
			$this->router = $this->buildRouter();
		}
		return $this->router;
	}

		protected function buildRouter():RouterServiceItf
		{
			return new RouterService(
				$this->buildRoutes()
			);
		}

			abstract protected function buildRoutes():array;

// -----------------------------------------------------------------------------

	private RequestBuilderServiceItf $requestBuilder;

	public function getRequestBuilder():RequestBuilderServiceItf
	{
		if (!isset($this->requestBuilder))
		{
			$this->requestBuilder = $this->buildRequestBuilder();
		}
		return $this->requestBuilder;
	}

		protected function buildRequestBuilder():RequestBuilderServiceItf
		{
			return new RequestBuilderService(
				$this->getAppBasePath(),
				$this->getStringReplacer()
			);
		}

			abstract protected function getAppBasePath():string;
	
// -----------------------------------------------------------------------------

	private ResponseDispatcherServiceItf $responseDispatcher;

	public function getResponseDispatcher():ResponseDispatcherServiceItf
	{
		if (!isset($this->responseDispatcher))
		{
			$this->responseDispatcher = $this->buildResponseDispatcher();
		}
		return $this->responseDispatcher;
	}

		protected function buildResponseDispatcher():ResponseDispatcherServiceItf
		{
			return new ResponseDispatcherService();
		}

// -----------------------------------------------------------------------------

	private CgiAppRunnerServiceItf $cgiAppRunner;

	public function getCgiAppRunner():CgiAppRunnerServiceItf
	{
		if (!isset($this->cgiAppRunner))
		{
			$this->cgiAppRunner = $this->buildCgiAppRunner();
		}
		return $this->cgiAppRunner;
	}

		protected function buildCgiAppRunner():CgiAppRunnerServiceItf
		{
			return new CgiAppRunnerService(
				$this->getRequestBuilder(),
				$this->getApp(),
				$this->getResponseDispatcher()
			);
		}

// -----------------------------------------------------------------------------

	private HtmlWidgetServiceItf $htmlWidget;

	public function getHtmlWidget():HtmlWidgetServiceItf
	{
		if (!isset($this->htmlWidget))
		{
			$this->htmlWidget = $this->buildHtmlWidget();
		}
		return $this->htmlWidget;
	}

		protected function buildHtmlWidget():HtmlWidgetServiceItf
		{
			return new HtmlWidgetService();
		}

}