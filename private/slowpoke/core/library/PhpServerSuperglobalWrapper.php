<?php
declare(strict_types=1);

namespace slowpoke\core\library;

use slowpoke\core\library\StringToStringMapWrapper;

final class PhpServerSuperglobalWrapper extends StringToStringMapWrapper
{

	const KEY_REQUEST_URI = 'REQUEST_URI';

	public function __construct(
	)
	{
		parent::__construct(
			$_SERVER
		);
	}

	public function hasRequestUri():bool
	{
		return $this->hasKey(static::KEY_REQUEST_URI);
	}

	public function getRequestUriAfterHas():string
	{
		return $this->getKeyAfterHas(static::KEY_REQUEST_URI);
	}

}