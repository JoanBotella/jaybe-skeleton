<?php
declare(strict_types=1);

namespace slowpoke\core\test;

use slowpoke\core\HelloWorld;
use PHPUnit\Framework\TestCase;

final class HelloWorldTest extends TestCase
{

	public function test_greet_returnsHelloWorld():void
	{
		$helloWorld = new HelloWorld();
		$this->assertEquals(
			'Hello World',
			$helloWorld->greet()
		);
	}

}
