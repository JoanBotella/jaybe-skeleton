<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\page\home\service\widget\mainContent;

use slowpoke\framework\library\widget\WidgetOutput;
use slowpoke\template\bundle\front\page\home\service\widget\mainContent\FrontHomeMainContentWidgetInput;

interface FrontHomeMainContentWidgetServiceItf
{

	public function run(FrontHomeMainContentWidgetInput $input):WidgetOutput;

}