<?php
declare(strict_types=1);

namespace slowpoke\core\library\serviceContainer;

use slowpoke\core\service\stringReplacer\StringReplacerServiceItf;
use slowpoke\core\service\stringReplacer\StringReplacerService;

trait CoreServiceContainerTrait
{

	private StringReplacerServiceItf $stringReplacer;

	public function getStringReplacer():StringReplacerServiceItf
	{
		if (!isset($this->stringReplacer))
		{
			$this->stringReplacer = $this->buildStringReplacer();
		}
		return $this->stringReplacer;
	}

		protected function buildStringReplacer():StringReplacerServiceItf
		{
			return new StringReplacerService();
		}

}