<?php
declare(strict_types=1);

namespace slowpoke\framework\service\app;

use slowpoke\framework\service\app\AppOutput;
use slowpoke\framework\service\app\AppInput;

interface AppServiceItf
{

	public function run(AppInput $input):AppOutput;

}