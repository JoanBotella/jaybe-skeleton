<?php
declare(strict_types=1);

namespace slowpoke\framework\service\router;

use slowpoke\core\library\serviceOutput\ServiceOutputAbs;
use slowpoke\framework\library\controller\ControllerServiceItf;

final class RouterOutput extends ServiceOutputAbs
{

	private ControllerServiceItf $controller;

	public function hasController():bool
	{
		return isset(
			$this->controller
		);
	}

	public function getControllerAfterHas():ControllerServiceItf
	{
		return $this->controller;
	}

	public function setController(ControllerServiceItf $v):void
	{
		$this->controller = $v;
	}

	public function unsetController():void
	{
		unset(
			$this->controller
		);
	}

}