<?php
declare(strict_types=1);

namespace slowpoke\framework\service\cgiAppRunner;

use slowpoke\framework\service\cgiAppRunner\CgiAppRunnerOutput;
use slowpoke\framework\service\cgiAppRunner\CgiAppRunnerInput;

interface CgiAppRunnerServiceItf
{

	public function run(CgiAppRunnerInput $input):CgiAppRunnerOutput;

}