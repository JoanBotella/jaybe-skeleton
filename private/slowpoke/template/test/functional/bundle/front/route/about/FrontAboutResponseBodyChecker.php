<?php
declare(strict_types=1);

namespace slowpoke\template\test\functional\bundle\front\route\about;

final class FrontAboutResponseBodyChecker
{

	public function check(string $body):bool
	{
		$expectedBody = <<<END
<!DOCTYPE html>
<html lang="en" data-page="front-about" class="front-about">
<head>
	<meta charset="UTF-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

	
	<title>About</title>

	
</head>
<body>

		
<div data-widget="front-layout" class="front-layout">

	<header></header>

	<nav></nav>

	<div>
		<main>
						
<h1>About</h1>
			
		</main>

		<aside></aside>
	</div>

	<footer></footer>

</div>
	
	
</body>
</html>
END;
		return $body == $expectedBody;
	}

}