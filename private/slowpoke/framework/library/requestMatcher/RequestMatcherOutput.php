<?php
declare(strict_types=1);

namespace slowpoke\framework\library\requestMatcher;

use slowpoke\core\library\serviceOutput\ServiceOutputAbs;

final class RequestMatcherOutput extends ServiceOutputAbs
{

	private bool $isMatch;

	public function hasIsMatch():bool
	{
		return isset(
			$this->isMatch
		);
	}

	public function getIsMatchAfterHas():bool
	{
		return $this->isMatch;
	}

	public function setIsMatch(bool $v):void
	{
		$this->isMatch = $v;
	}

	public function unsetIsMatch():void
	{
		unset(
			$this->isMatch
		);
	}

}