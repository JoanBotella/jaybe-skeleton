<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\page\notFound\service\responseBuilder;

use slowpoke\template\bundle\front\library\responseBuilder\FrontResponseBuilderServiceAbs;
use slowpoke\template\bundle\front\page\notFound\service\responseBuilder\FrontNotFoundResponseBuilderServiceItf;
use slowpoke\template\bundle\front\page\notFound\service\responseBuilder\FrontNotFoundResponseBuilderInput;
use slowpoke\framework\library\responseBuilder\ResponseBuilderOutput;
use slowpoke\framework\service\widget\html\HtmlWidgetServiceItf;
use slowpoke\framework\service\widget\html\HtmlWidgetInput;
use slowpoke\template\bundle\front\service\widget\layout\FrontLayoutWidgetServiceItf;
use slowpoke\template\bundle\front\page\notFound\service\widget\mainContent\FrontNotFoundMainContentWidgetServiceItf;
use slowpoke\template\bundle\front\page\notFound\service\widget\mainContent\FrontNotFoundMainContentWidgetInput;
use slowpoke\core\library\HttpStatusConstant;
use slowpoke\core\library\Error;

final class FrontNotFoundResponseBuilderService extends FrontResponseBuilderServiceAbs implements FrontNotFoundResponseBuilderServiceItf
{
	const ERROR_CODE_NO_MAIN_CONTENT_WIDGET_TEXT = 0;

	private FrontNotFoundMainContentWidgetServiceItf $mainContentWidget;

	private FrontNotFoundResponseBuilderInput $input;

	public function __construct(
		HtmlWidgetServiceItf $htmlWidget,
		FrontLayoutWidgetServiceItf $layoutWidget,
		FrontNotFoundMainContentWidgetServiceItf $mainContentWidget
	)
	{
		parent::__construct(
			$htmlWidget,
			$layoutWidget
		);
		$this->mainContentWidget = $mainContentWidget;
	}

	public function run(FrontNotFoundResponseBuilderInput $input):ResponseBuilderOutput
	{
		$this->input = $input;

		$this->setupOutput();

		$output = $this->output;

		$this->tearDown();

		return $output;
	}

	protected function tryToSetupMainContentWidgetText():void
	{
		$mainContentWidgetOutput = $this->mainContentWidget->run(
			$this->buildMainContentWidgetInput()
		);

		if ($mainContentWidgetOutput->hasErrors())
		{
			$this->output->addErrors(
				$mainContentWidgetOutput->getErrorsAfterHas()
			);
			return;
		}

		if ($mainContentWidgetOutput->hasText())
		{
			$this->mainContentWidgetText = $mainContentWidgetOutput->getTextAfterHas();
			return;
		}

		$this->output->addError(
			new Error(
				self::ERROR_CODE_NO_MAIN_CONTENT_WIDGET_TEXT,
				'MainContentWidget had no errors but had no text'
			)
		);
	}

		private function buildMainContentWidgetInput():FrontNotFoundMainContentWidgetInput
		{
			$mainContentWidgetInput = new FrontNotFoundMainContentWidgetInput(
			);
			return $mainContentWidgetInput;
		}

	protected function buildHtmlWidgetInput():HtmlWidgetInput
	{
		// !!!

		$htmlWidgetInput = new HtmlWidgetInput(
			'en',
			'Not Found'
		);

		$htmlWidgetInput->setPageCode('front-not_found');

		$htmlWidgetInput->setBodyContent(
			$this->layoutWidgetText
		);

		return $htmlWidgetInput;
	}

	protected function getHttpStatus():int
	{
		return HttpStatusConstant::NOT_FOUND;
	}

	protected function tearDown():void
	{
		unset($this->input);
		parent::tearDown();
	}

}