<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\library\serviceContainer;

use slowpoke\template\bundle\front\page\about\library\serviceContainer\FrontAboutServiceContainerItf;
use slowpoke\template\bundle\front\page\home\library\serviceContainer\FrontHomeServiceContainerItf;
use slowpoke\template\bundle\front\page\notFound\library\serviceContainer\FrontNotFoundServiceContainerItf;

use slowpoke\template\bundle\front\service\widget\layout\FrontLayoutWidgetServiceItf;

interface FrontServiceContainerItf
extends
	FrontAboutServiceContainerItf,
	FrontHomeServiceContainerItf,
	FrontNotFoundServiceContainerItf
{

	public function getFrontLayoutWidget():FrontLayoutWidgetServiceItf;

}