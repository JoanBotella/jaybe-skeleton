<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\page\about\service\responseBuilder;

use slowpoke\framework\library\responseBuilder\ResponseBuilderOutput;
use slowpoke\template\bundle\front\page\about\service\responseBuilder\FrontAboutResponseBuilderInput;

interface FrontAboutResponseBuilderServiceItf
{

	public function run(FrontAboutResponseBuilderInput $input):ResponseBuilderOutput;

}