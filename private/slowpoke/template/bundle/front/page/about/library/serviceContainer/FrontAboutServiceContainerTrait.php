<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\page\about\library\serviceContainer;

use slowpoke\framework\library\controller\ControllerServiceItf;
use slowpoke\framework\library\requestMatcher\RequestMatcherServiceItf;
use slowpoke\template\bundle\front\page\about\service\requestMatcher\FrontAboutRequestMatcherService;
use slowpoke\template\bundle\front\page\about\service\controller\FrontAboutControllerService;
use slowpoke\template\bundle\front\page\about\service\responseBuilder\FrontAboutResponseBuilderServiceItf;
use slowpoke\template\bundle\front\page\about\service\responseBuilder\FrontAboutResponseBuilderService;
use slowpoke\template\bundle\front\page\about\service\widget\mainContent\FrontAboutMainContentWidgetServiceItf;
use slowpoke\template\bundle\front\page\about\service\widget\mainContent\FrontAboutMainContentWidgetService;

trait FrontAboutServiceContainerTrait
{

	private RequestMatcherServiceItf $frontAboutRequestMatcher;

	public function getFrontAboutRequestMatcher():RequestMatcherServiceItf
	{
		if (!isset($this->frontAboutRequestMatcher))
		{
			$this->frontAboutRequestMatcher = $this->buildFrontAboutRequestMatcher();
		}
		return $this->frontAboutRequestMatcher;
	}

		protected function buildFrontAboutRequestMatcher():RequestMatcherServiceItf
		{
			return new FrontAboutRequestMatcherService();
		}

// -----------------------------------------------------------------------------

	private ControllerServiceItf $frontAboutController;

	public function getFrontAboutController():ControllerServiceItf
	{
		if (!isset($this->frontAboutController))
		{
			$this->frontAboutController = $this->buildFrontAboutController();
		}
		return $this->frontAboutController;
	}

		protected function buildFrontAboutController():ControllerServiceItf
		{
			return new FrontAboutControllerService(
				$this->getFrontAboutResponseBuilder()
			);
		}

// -----------------------------------------------------------------------------

	private FrontAboutResponseBuilderServiceItf $frontAboutResponseBuilder;

	public function getFrontAboutResponseBuilder():FrontAboutResponseBuilderServiceItf
	{
		if (!isset($this->frontAboutResponseBuilder))
		{
			$this->frontAboutResponseBuilder = $this->buildFrontAboutResponseBuilder();
		}
		return $this->frontAboutResponseBuilder;
	}

		protected function buildFrontAboutResponseBuilder():FrontAboutResponseBuilderServiceItf
		{
			return new FrontAboutResponseBuilderService(
				$this->getHtmlWidget(),
				$this->getFrontLayoutWidget(),
				$this->getFrontAboutMainContentWidget()
			);
		}

// -----------------------------------------------------------------------------

	private FrontAboutMainContentWidgetServiceItf $frontAboutMainContentWidget;

	public function getFrontAboutMainContentWidget():FrontAboutMainContentWidgetServiceItf
	{
		if (!isset($this->frontAboutMainContentWidget))
		{
			$this->frontAboutMainContentWidget = $this->buildFrontAboutMainContentWidget();
		}
		return $this->frontAboutMainContentWidget;
	}

		protected function buildFrontAboutMainContentWidget():FrontAboutMainContentWidgetServiceItf
		{
			return new FrontAboutMainContentWidgetService();
		}

}