<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\page\notFound\library\serviceContainer;

use slowpoke\framework\library\controller\ControllerServiceItf;
use slowpoke\framework\library\requestMatcher\RequestMatcherServiceItf;
use slowpoke\template\bundle\front\page\notFound\service\requestMatcher\FrontNotFoundRequestMatcherService;
use slowpoke\template\bundle\front\page\notFound\service\controller\FrontNotFoundControllerService;
use slowpoke\template\bundle\front\page\notFound\service\responseBuilder\FrontNotFoundResponseBuilderServiceItf;
use slowpoke\template\bundle\front\page\notFound\service\responseBuilder\FrontNotFoundResponseBuilderService;
use slowpoke\template\bundle\front\page\notFound\service\widget\mainContent\FrontNotFoundMainContentWidgetServiceItf;
use slowpoke\template\bundle\front\page\notFound\service\widget\mainContent\FrontNotFoundMainContentWidgetService;

trait FrontNotFoundServiceContainerTrait
{

	private RequestMatcherServiceItf $frontNotFoundRequestMatcher;

	public function getFrontNotFoundRequestMatcher():RequestMatcherServiceItf
	{
		if (!isset($this->frontNotFoundRequestMatcher))
		{
			$this->frontNotFoundRequestMatcher = $this->buildFrontNotFoundRequestMatcher();
		}
		return $this->frontNotFoundRequestMatcher;
	}

		protected function buildFrontNotFoundRequestMatcher():RequestMatcherServiceItf
		{
			return new FrontNotFoundRequestMatcherService();
		}

// -----------------------------------------------------------------------------

	private ControllerServiceItf $frontNotFoundController;

	public function getFrontNotFoundController():ControllerServiceItf
	{
		if (!isset($this->frontNotFoundController))
		{
			$this->frontNotFoundController = $this->buildFrontNotFoundController();
		}
		return $this->frontNotFoundController;
	}

		protected function buildFrontNotFoundController():ControllerServiceItf
		{
			return new FrontNotFoundControllerService(
				$this->getFrontNotFoundResponseBuilder()
			);
		}

// -----------------------------------------------------------------------------

	private FrontNotFoundResponseBuilderServiceItf $frontNotFoundResponseBuilder;

	public function getFrontNotFoundResponseBuilder():FrontNotFoundResponseBuilderServiceItf
	{
		if (!isset($this->frontNotFoundResponseBuilder))
		{
			$this->frontNotFoundResponseBuilder = $this->buildFrontNotFoundResponseBuilder();
		}
		return $this->frontNotFoundResponseBuilder;
	}

		protected function buildFrontNotFoundResponseBuilder():FrontNotFoundResponseBuilderServiceItf
		{
			return new FrontNotFoundResponseBuilderService(
				$this->getHtmlWidget(),
				$this->getFrontLayoutWidget(),
				$this->getFrontNotFoundMainContentWidget()
			);
		}

// -----------------------------------------------------------------------------

	private FrontNotFoundMainContentWidgetServiceItf $frontNotFoundMainContentWidget;

	public function getFrontNotFoundMainContentWidget():FrontNotFoundMainContentWidgetServiceItf
	{
		if (!isset($this->frontNotFoundMainContentWidget))
		{
			$this->frontNotFoundMainContentWidget = $this->buildFrontNotFoundMainContentWidget();
		}
		return $this->frontNotFoundMainContentWidget;
	}

		protected function buildFrontNotFoundMainContentWidget():FrontNotFoundMainContentWidgetServiceItf
		{
			return new FrontNotFoundMainContentWidgetService();
		}

}