<?php
declare(strict_types=1);

namespace slowpoke\core\test\unitary\service\stringReplacer;

use PHPUnit\Framework\TestCase;
use slowpoke\core\test\unitary\service\stringReplacer\InputOutputPair;
use slowpoke\core\test\unitary\service\stringReplacer\InputOutputPairsBuilder;
use slowpoke\core\service\stringReplacer\StringReplacerServiceItf;
use slowpoke\core\service\stringReplacer\StringReplacerService;

final class HelloWorldTest extends TestCase
{

	private StringReplacerServiceItf $stringReplacer;

	private array $inputOutputPairs;

	public function __construct(
		$name = null,
		$data = [],
		$dataName = ''
	)
	{
		parent::__construct(
			$name,
			$data,
			$dataName
		);
		$this->stringReplacer = new StringReplacerService();
		$this->inputOutputPairs = $this->buildInputOutputPairs();
	}

		private function buildInputOutputPairs():array
		{
			$builder = new InputOutputPairsBuilder();
			return $builder->build();
		}

	public function test_stringReplacer_hasText():void
	{
		foreach ($this->inputOutputPairs as $inputOutputPair)
		{
			$this->assertThatStringReplacerOutputHasTextByInputOutputPair($inputOutputPair);
		}
	}

		private function assertThatStringReplacerOutputHasTextByInputOutputPair(InputOutputPair $inputOutputPair):void
		{
			$output = $this->stringReplacer->run(
				$inputOutputPair->getInput()
			);
			$this->assertTrue(
				$output->hasText()
			);
		}

	public function test_stringReplacer_outputsExpectedText():void
	{
		foreach ($this->inputOutputPairs as $inputOutputPair)
		{
			$this->assertThatStringReplacerOutputTextIsTheExpectedTextByInputOutputPair($inputOutputPair);
		}
	}

		private function assertThatStringReplacerOutputTextIsTheExpectedTextByInputOutputPair(InputOutputPair $inputOutputPair):void
		{
			$output = $this->stringReplacer->run(
				$inputOutputPair->getInput()
			);
			$this->assertEquals(
				$output->getTextAfterHas(),
				$inputOutputPair->getOutput()->getTextAfterHas()
			);
		}

}
