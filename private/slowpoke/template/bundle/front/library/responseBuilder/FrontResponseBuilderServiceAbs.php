<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\library\responseBuilder;

use slowpoke\template\library\responseBuilder\ResponseBuilderServiceAbs;
use slowpoke\framework\service\widget\html\HtmlWidgetServiceItf;
use slowpoke\template\bundle\front\service\widget\layout\FrontLayoutWidgetServiceItf;
use slowpoke\template\bundle\front\service\widget\layout\FrontLayoutWidgetInput;

abstract class FrontResponseBuilderServiceAbs extends ResponseBuilderServiceAbs
{

	private FrontLayoutWidgetServiceItf $layoutWidget;

	public function __construct(
		HtmlWidgetServiceItf $htmlWidget,
		FrontLayoutWidgetServiceItf $layoutWidget
	)
	{
		parent::__construct(
			$htmlWidget
		);
		$this->layoutWidget = $layoutWidget;
	}

	protected function tryToSetupLayoutWidgetText():void
	{
		$layoutWidgetOutput = $this->layoutWidget->run(
			$this->buildLayoutWidgetInput()
		);

		if ($layoutWidgetOutput->hasErrors())
		{
			$this->output->addErrors(
				$layoutWidgetOutput->getErrorsAfterHas()
			);
			return;
		}

		if ($layoutWidgetOutput->hasText())
		{
			$this->layoutWidgetText = $layoutWidgetOutput->getTextAfterHas();
			return;
		}

		$this->output->addError(
			new Error('layoutWidget had no errors but had no text')
		);
	}

		private function buildLayoutWidgetInput():FrontLayoutWidgetInput
		{
			$layoutWidgetInput = new FrontLayoutWidgetInput(
			);

			$layoutWidgetInput->setMainContent(
				$this->mainContentWidgetText
			);

			return $layoutWidgetInput;
		}

}