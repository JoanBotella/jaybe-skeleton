<?php
declare(strict_types=1);

namespace slowpoke\core\service\stringReplacer;

use slowpoke\core\service\stringReplacer\StringReplacerOutput;
use slowpoke\core\service\stringReplacer\StringReplacerInput;

interface StringReplacerServiceItf
{

	public function run(StringReplacerInput $input):StringReplacerOutput;

}