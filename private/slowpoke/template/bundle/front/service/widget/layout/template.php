<?php
declare(strict_types=1);

use slowpoke\template\bundle\front\service\widget\layout\FrontLayoutWidgetInput;

/**
 * @var FrontLayoutWidgetInput $input
 */

?>

<div data-widget="front-layout" class="front-layout">

	<header></header>

	<nav></nav>

	<div>
		<main>
			<?php if ($input->hasMainContent()): ?>
			<?= $input->getMainContentAfterHas() ?>
			<?php endif; ?>

		</main>

		<aside></aside>
	</div>

	<footer></footer>

</div>
