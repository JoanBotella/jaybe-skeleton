<?php
declare(strict_types=1);

namespace slowpoke\framework\service\app;

use slowpoke\framework\service\app\AppOutput;
use slowpoke\framework\service\app\AppInput;
use slowpoke\framework\service\app\AppServiceItf;
use slowpoke\framework\service\router\RouterInput;
use slowpoke\framework\service\router\RouterServiceItf;
use slowpoke\framework\library\response\Response;
use slowpoke\framework\library\controller\ControllerInput;
use slowpoke\framework\library\controller\ControllerServiceItf;
use slowpoke\core\library\Error;

final class AppService implements AppServiceItf
{
	const ERROR_CODE_NO_CONTROLLER_RESPONSE = 0;
	const ERROR_CODE_NO_ROUTER_CONTROLLER = 1;

	private RouterServiceItf $router;

	private AppInput $input;

	private AppOutput $output;

	private Response $response;

	private ControllerServiceItf $controller;

	public function __construct(
		RouterServiceItf $router
	)
	{
		$this->router = $router;
	}

	public function run(AppInput $input):AppOutput
	{
		$this->input = $input;

		$this->setupOutput();

		$output = $this->output;

		$this->tearDown();

		return $output;
	}

		private function setupOutput():void
		{
			$this->output = $this->buildOutput();
			$this->tryToSetupResponseToOutput();
		}

			private function buildOutput():AppOutput
			{
				return new AppOutput();
			}

			private function tryToSetupResponseToOutput():void
			{
				$this->tryToSetupResponse();

				if ($this->output->hasErrors())
				{
					return;
				}

				$this->output->setResponse(
					$this->response
				);
			}

				private function tryToSetupResponse():void
				{
					$this->tryToSetupController();

					if ($this->output->hasErrors())
					{
						return;
					}

					$controllerInput = new ControllerInput(
						$this->input->getRequest()
					);

					$controllerOutput = $this->controller->action(
						$controllerInput
					);

					if ($controllerOutput->hasErrors())
					{
						$this->output->addErrors(
							$controllerOutput->getErrorsAfterHas()
						);
						return;
					}

					if ($controllerOutput->hasResponse())
					{
						$this->response = $controllerOutput->getResponseAfterHas();
						return;
					}

					$this->output->addError(
						new Error(
							self::ERROR_CODE_NO_CONTROLLER_RESPONSE,
							'Controller had no errors but had no response'
						)
					);
				}

					private function tryToSetupController():void
					{
						$routerInput = new RouterInput(
							$this->input->getRequest()
						);

						$routerOutput = $this->router->route(
							$routerInput
						);

						if ($routerOutput->hasErrors())
						{
							$this->output->addErrors(
								$routerOutput->getErrorsAfterHas()
							);
							return;
						}

						if ($routerOutput->hasController())
						{
							$this->controller = $routerOutput->getControllerAfterHas();
							return;
						}

						$this->output->addError(
							new Error(
								self::ERROR_CODE_NO_ROUTER_CONTROLLER,
								'Router had no errors but had no controller'
							)
						);
					}

		private function tearDown():void
		{
			unset($this->input);
			unset($this->output);
			unset($this->response);
		}

}