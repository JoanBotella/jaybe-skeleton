<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\page\home\library\serviceContainer;

use slowpoke\framework\library\controller\ControllerServiceItf;
use slowpoke\framework\library\requestMatcher\RequestMatcherServiceItf;
use slowpoke\template\bundle\front\page\home\service\requestMatcher\FrontHomeRequestMatcherService;
use slowpoke\template\bundle\front\page\home\service\controller\FrontHomeControllerService;
use slowpoke\template\bundle\front\page\home\service\responseBuilder\FrontHomeResponseBuilderServiceItf;
use slowpoke\template\bundle\front\page\home\service\responseBuilder\FrontHomeResponseBuilderService;
use slowpoke\template\bundle\front\page\home\service\widget\mainContent\FrontHomeMainContentWidgetServiceItf;
use slowpoke\template\bundle\front\page\home\service\widget\mainContent\FrontHomeMainContentWidgetService;

trait FrontHomeServiceContainerTrait
{

	private RequestMatcherServiceItf $frontHomeRequestMatcher;

	public function getFrontHomeRequestMatcher():RequestMatcherServiceItf
	{
		if (!isset($this->frontHomeRequestMatcher))
		{
			$this->frontHomeRequestMatcher = $this->buildFrontHomeRequestMatcher();
		}
		return $this->frontHomeRequestMatcher;
	}

		protected function buildFrontHomeRequestMatcher():RequestMatcherServiceItf
		{
			return new FrontHomeRequestMatcherService();
		}

// -----------------------------------------------------------------------------

	private ControllerServiceItf $frontHomeController;

	public function getFrontHomeController():ControllerServiceItf
	{
		if (!isset($this->frontHomeController))
		{
			$this->frontHomeController = $this->buildFrontHomeController();
		}
		return $this->frontHomeController;
	}

		protected function buildFrontHomeController():ControllerServiceItf
		{
			return new FrontHomeControllerService(
				$this->getFrontHomeResponseBuilder()
			);
		}

// -----------------------------------------------------------------------------

	private FrontHomeResponseBuilderServiceItf $frontHomeResponseBuilder;

	public function getFrontHomeResponseBuilder():FrontHomeResponseBuilderServiceItf
	{
		if (!isset($this->frontHomeResponseBuilder))
		{
			$this->frontHomeResponseBuilder = $this->buildFrontHomeResponseBuilder();
		}
		return $this->frontHomeResponseBuilder;
	}

		protected function buildFrontHomeResponseBuilder():FrontHomeResponseBuilderServiceItf
		{
			return new FrontHomeResponseBuilderService(
				$this->getHtmlWidget(),
				$this->getFrontLayoutWidget(),
				$this->getFrontHomeMainContentWidget()
			);
		}

// -----------------------------------------------------------------------------

	private FrontHomeMainContentWidgetServiceItf $frontHomeMainContentWidget;

	public function getFrontHomeMainContentWidget():FrontHomeMainContentWidgetServiceItf
	{
		if (!isset($this->frontHomeMainContentWidget))
		{
			$this->frontHomeMainContentWidget = $this->buildFrontHomeMainContentWidget();
		}
		return $this->frontHomeMainContentWidget;
	}

		protected function buildFrontHomeMainContentWidget():FrontHomeMainContentWidgetServiceItf
		{
			return new FrontHomeMainContentWidgetService();
		}

}