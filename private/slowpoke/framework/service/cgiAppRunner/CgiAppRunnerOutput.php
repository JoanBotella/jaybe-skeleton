<?php
declare(strict_types=1);

namespace slowpoke\framework\service\cgiAppRunner;

use slowpoke\core\library\serviceOutput\ServiceOutputAbs;

final class CgiAppRunnerOutput extends ServiceOutputAbs
{
}