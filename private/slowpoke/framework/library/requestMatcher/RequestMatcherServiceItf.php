<?php
declare(strict_types=1);

namespace slowpoke\framework\library\requestMatcher;

use slowpoke\framework\library\requestMatcher\RequestMatcherInput;
use slowpoke\framework\library\requestMatcher\RequestMatcherOutput;

interface RequestMatcherServiceItf
{

	public function match(RequestMatcherInput $input):RequestMatcherOutput;

}