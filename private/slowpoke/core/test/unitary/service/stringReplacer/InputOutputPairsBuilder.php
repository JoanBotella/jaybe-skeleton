<?php
declare(strict_types=1);

namespace slowpoke\core\test\unitary\service\stringReplacer;

use slowpoke\core\service\stringReplacer\StringReplacerInput;
use slowpoke\core\service\stringReplacer\StringReplacerOutput;
use slowpoke\core\test\unitary\service\stringReplacer\InputOutputPair;

final class InputOutputPairsBuilder
{

	public function build():array
	{
		$pairs = [];

		$output = new StringReplacerOutput();
		$output->setText('bbcd');
		$pairs[] = new InputOutputPair(
			new StringReplacerInput(
				'a',
				'b',
				'abcd'
			),
			$output
		);

		$output = new StringReplacerOutput();
		$output->setText('You should eat pizza, vegetables, and fiber every day.');
		$pairs[] = new InputOutputPair(
			new StringReplacerInput(
				'fruits',
				'pizza',
				'You should eat fruits, vegetables, and fiber every day.'
			),
			$output
		);

		$output = new StringReplacerOutput();
		$output->setText('Red blue Red blue.');
		$pairs[] = new InputOutputPair(
			new StringReplacerInput(
				'Blue',
				'Red',
				'Blue blue Blue blue.'
			),
			$output
		);

		$output = new StringReplacerOutput();
		$output->setText('Red red Red red.');
		$pairs[] = new InputOutputPair(
			new StringReplacerInput(
				'  ',
				' ',
				'Red  red Red  red.'
			),
			$output
		);

		$output = new StringReplacerOutput();
		$output->setText('important_database_table');
		$pairs[] = new InputOutputPair(
			new StringReplacerInput(
				'prefix_',
				'',
				'prefix_important_database_table'
			),
			$output
		);

		return $pairs;
	}

}