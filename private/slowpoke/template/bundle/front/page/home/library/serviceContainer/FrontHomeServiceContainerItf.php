<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\page\home\library\serviceContainer;

use slowpoke\framework\library\controller\ControllerServiceItf;
use slowpoke\framework\library\requestMatcher\RequestMatcherServiceItf;
use slowpoke\template\bundle\front\page\home\service\responseBuilder\FrontHomeResponseBuilderServiceItf;
use slowpoke\template\bundle\front\page\home\service\widget\mainContent\FrontHomeMainContentWidgetServiceItf;

interface FrontHomeServiceContainerItf
{

	public function getFrontHomeRequestMatcher():RequestMatcherServiceItf;

	public function getFrontHomeController():ControllerServiceItf;

	public function getFrontHomeResponseBuilder():FrontHomeResponseBuilderServiceItf;

	public function getFrontHomeMainContentWidget():FrontHomeMainContentWidgetServiceItf;

}