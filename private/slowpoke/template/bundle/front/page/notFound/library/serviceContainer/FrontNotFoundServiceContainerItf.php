<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\page\notFound\library\serviceContainer;

use slowpoke\framework\library\controller\ControllerServiceItf;
use slowpoke\framework\library\requestMatcher\RequestMatcherServiceItf;
use slowpoke\template\bundle\front\page\notFound\service\responseBuilder\FrontNotFoundResponseBuilderServiceItf;
use slowpoke\template\bundle\front\page\notFound\service\widget\mainContent\FrontNotFoundMainContentWidgetServiceItf;

interface FrontNotFoundServiceContainerItf
{

	public function getFrontNotFoundRequestMatcher():RequestMatcherServiceItf;

	public function getFrontNotFoundController():ControllerServiceItf;

	public function getFrontNotFoundResponseBuilder():FrontNotFoundResponseBuilderServiceItf;

	public function getFrontNotFoundMainContentWidget():FrontNotFoundMainContentWidgetServiceItf;

}