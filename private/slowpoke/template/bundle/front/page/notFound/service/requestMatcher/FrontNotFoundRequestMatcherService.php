<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\page\notFound\service\requestMatcher;

use slowpoke\framework\library\requestMatcher\RequestMatcherServiceAbs;

final class FrontNotFoundRequestMatcherService extends RequestMatcherServiceAbs
{

	protected function setupIsMatch():void
	{
		$this->isMatch = true;
	}

}