<?php
declare(strict_types=1);

namespace slowpoke\core\library;

final class HttpStatusConstant
{

	const OK = 200;

	const NOT_FOUND = 404;

}