<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\page\about\service\controller;

use slowpoke\framework\library\controller\ControllerServiceAbs;
use slowpoke\template\bundle\front\page\about\service\responseBuilder\FrontAboutResponseBuilderServiceItf;
use slowpoke\template\bundle\front\page\about\service\responseBuilder\FrontAboutResponseBuilderInput;
use slowpoke\core\library\Error;

final class FrontAboutControllerService extends ControllerServiceAbs
{
	const ERROR_CODE_NO_RESPONSE_BUILDER_RESPONSE = 0;

	private FrontAboutResponseBuilderServiceItf $responseBuilder;

	public function __construct(
		FrontAboutResponseBuilderServiceItf $responseBuilder
	)
	{
		$this->responseBuilder = $responseBuilder;
	}

	protected function setupResponse():void
	{
		$responseBuilderInput = new FrontAboutResponseBuilderInput();

		$responseBuilderOutput = $this->responseBuilder->run(
			$responseBuilderInput
		);

		if ($responseBuilderOutput->hasErrors())
		{
			$this->output->addErrors(
				$responseBuilderOutput->getErrorsAfterHas()
			);
			return;
		}

		if ($responseBuilderOutput->hasResponse())
		{
			$this->response = $responseBuilderOutput->getResponseAfterHas();
			return;
		}

		$this->output->addError(
			new Error(
				self::ERROR_CODE_NO_RESPONSE_BUILDER_RESPONSE,
				'ResponseBuilder had no errors but had no response'
			)
		);
	}

}