<?php
declare(strict_types=1);

namespace slowpoke\template\test;

use slowpoke\template\HelloWorld;
use PHPUnit\Framework\TestCase;

final class HelloWorldTest extends TestCase
{

	public function test_greet_returnsHelloWorld():void
	{
		$helloWorld = new HelloWorld();
		$this->assertEquals(
			'Hello World',
			$helloWorld->greet()
		);
	}

}
