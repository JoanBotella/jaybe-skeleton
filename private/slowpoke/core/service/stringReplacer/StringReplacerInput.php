<?php
declare(strict_types=1);

namespace slowpoke\core\service\stringReplacer;

final class StringReplacerInput
{
	private string $search;

	private string $replacement;

	private string $text;

	public function __construct(
		string $search,
		string $replacement,
		string $text
	)
	{
		$this->search = $search;
		$this->replacement = $replacement;
		$this->text = $text;
	}

	public function getSearch():string
	{
		return $this->search;
	}

	public function getReplacement():string
	{
		return $this->replacement;
	}

	public function getText():string
	{
		return $this->text;
	}

}