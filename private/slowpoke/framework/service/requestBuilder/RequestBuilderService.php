<?php
declare(strict_types=1);

namespace slowpoke\framework\service\requestBuilder;

use slowpoke\core\service\stringReplacer\StringReplacerServiceItf;
use slowpoke\core\service\stringReplacer\StringReplacerInput;
use slowpoke\framework\service\requestBuilder\RequestBuilderServiceItf;
use slowpoke\framework\service\requestBuilder\RequestBuilderInput;
use slowpoke\framework\service\requestBuilder\RequestBuilderOutput;
use slowpoke\framework\library\request\Request;
use slowpoke\core\library\PhpServerSuperglobalWrapper;
use slowpoke\core\library\Error;

final class RequestBuilderService implements RequestBuilderServiceItf
{
	const ERROR_CODE_NO_PHP_SERVER_SUPERGLOBAL_REQUEST_URI = 0;
	const ERROR_CODE_NO_STRING_REPLACER_TEXT = 1;

	private string $appBasePath;

	private StringReplacerServiceItf $stringReplacer;

	private RequestBuilderInput $input;

	private RequestBuilderOutput $output;

	private Request $request;

	private string $requestUri;

	private string $path;

	public function __construct(
		string $appBasePath,
		StringReplacerServiceItf $stringReplacer
	)
	{
		$this->appBasePath = $appBasePath;
		$this->stringReplacer = $stringReplacer;
	}

	public function run(RequestBuilderInput $input):RequestBuilderOutput
	{
		$this->input = $input;

		$this->setupOutput();

		$output = $this->output;

		$this->tearDown();

		return $output;
	}

		private function setupOutput():void
		{
			$this->output = $this->buildOutput();

			$this->tryToSetupRequest();

			if ($this->output->hasErrors())
			{
				return;
			}

			$this->output->setRequest(
				$this->request
			);
		}

			private function buildOutput():RequestBuilderOutput
			{
				return new RequestBuilderOutput();
			}

			private function tryToSetupRequest():void
			{
				$this->tryToSetupRequestUri();

				if ($this->output->hasErrors())
				{
					return;
				}

				$this->tryToSetupPath();

				if ($this->output->hasErrors())
				{
					return;
				}

				$this->request = new Request(
					$this->path
				);
			}

				private function tryToSetupRequestUri():void
				{
					$phpServerSuperglobalWrapper = new PhpServerSuperglobalWrapper();

					if ($phpServerSuperglobalWrapper->hasRequestUri())
					{
						$this->requestUri = $phpServerSuperglobalWrapper->getRequestUriAfterHas();
						return;
					}

					$this->output->addError(
						new Error(
							self::ERROR_CODE_NO_PHP_SERVER_SUPERGLOBAL_REQUEST_URI,
							'Php Server superglobal does not have REQUEST_URI'
						)
					);
				}

				private function tryToSetupPath():void
				{
					$stringReplacerInput = new StringReplacerInput(
						$this->appBasePath,
						'',
						$this->requestUri
					);

					$stringReplacerOutput = $this->stringReplacer->run(
						$stringReplacerInput
					);

					if ($stringReplacerOutput->hasErrors())
					{
						$this->output->addErrors(
							$stringReplacerOutput->getErrors()
						);
						return;
					}

					if ($stringReplacerOutput->hasText())
					{
						$this->path = $stringReplacerOutput->getTextAfterHas();
						return;
					}

					$this->output->addError(
						new Error(
							self::ERROR_CODE_NO_STRING_REPLACER_TEXT,
							'StringReplacer had no errors but had no text'
						)
					);
				}

		private function tearDown():void
		{
			unset($this->input);
			unset($this->output);
			unset($this->request);
			unset($this->requestUri);
			unset($this->path);
		}

}