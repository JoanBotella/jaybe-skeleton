<?php
declare(strict_types=1);

namespace slowpoke\framework\service\widget\html;

use slowpoke\framework\library\widget\WidgetOutput;
use slowpoke\framework\service\widget\html\HtmlWidgetInput;

interface HtmlWidgetServiceItf
{

	public function run(HtmlWidgetInput $input):WidgetOutput;

}