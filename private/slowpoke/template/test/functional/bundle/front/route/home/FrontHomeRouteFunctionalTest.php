<?php
declare(strict_types=1);

namespace slowpoke\template\test\functional\bundle\front\route\home;

use PHPUnit\Framework\TestCase;
use slowpoke\framework\library\request\Request;
use slowpoke\framework\service\app\AppInput;
use slowpoke\framework\service\app\AppOutput;
use slowpoke\core\library\HttpStatusConstant;
use slowpoke\template\library\serviceContainer\ServiceContainer;
use slowpoke\template\library\serviceContainer\ServiceContainerItf;
use slowpoke\template\test\functional\bundle\front\route\home\FrontHomeResponseBodyChecker;

final class FrontHomeRouteFunctionalTest extends TestCase
{

	private ServiceContainerItf $serviceContainer;

	public function __construct(
		$name = null,
		$data = [],
		$dataName = ''
	)
	{
		parent::__construct(
			$name,
			$data,
			$dataName
		);
		$this->serviceContainer = new ServiceContainer();
	}

	public function test_requestingFrontHome_returnsAnOutputWithoutErrors():void
	{
		$output = $this->runAppWithRequestToFrontHome();
		$this->assertFalse(
			$output->hasErrors()
		);
	}

		private function runAppWithRequestToFrontHome():AppOutput
		{
			$app = $this->serviceContainer->getApp();
			return $app->run(
				$this->buildAppInputWithRequestToFrontHome()
			);
		}

			private function buildAppInputWithRequestToFrontHome():AppInput
			{
				$input = new AppInput(
					$this->buildRequestToFrontHome()
				);
				return $input;
			}

				private function buildRequestToFrontHome():Request
				{
					return new Request('');
				}

	public function test_requestingFrontHome_outputsAResponse():void
	{
		$output = $this->runAppWithRequestToFrontHome();
		$this->assertTrue(
			$output->hasResponse()
		);
	}

	public function test_requestingFrontHome_outputsAResponseWithOkHttpStatus():void
	{
		$output = $this->runAppWithRequestToFrontHome();
		$response = $output->getResponseAfterHas();
		$this->assertEquals(
			HttpStatusConstant::OK,
			$response->getHttpStatus()
		);
	}

	public function test_requestingFrontHome_outputsTheExpectedResponseBody():void
	{
		$output = $this->runAppWithRequestToFrontHome();
		$response = $output->getResponseAfterHas();
		$responseBodyChecker = new FrontHomeResponseBodyChecker();
		$this->assertTrue(
			$responseBodyChecker->check(
				$response->getBody()
			)
		);
	}

}
