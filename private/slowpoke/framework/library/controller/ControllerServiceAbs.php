<?php
declare(strict_types=1);

namespace slowpoke\framework\library\controller;

use slowpoke\framework\library\controller\ControllerServiceItf;
use slowpoke\framework\library\controller\ControllerInput;
use slowpoke\framework\library\controller\ControllerOutput;
use slowpoke\framework\library\response\Response;
use slowpoke\framework\library\request\Request;

abstract class ControllerServiceAbs implements ControllerServiceItf
{

	private ControllerInput $input;

	private ControllerOutput $output;

	protected Response $response;

	public function action(ControllerInput $input):ControllerOutput
	{
		$this->input = $input;

		$this->setupOutput();

		$output = $this->output;

		$this->tearDown();

		return $output;
	}

		private function setupOutput():void
		{
			$this->output = $this->buildOutput();

			$this->setupResponse();

			if (isset($this->response))
			{
				$this->output->setResponse(
					$this->response
				);
			}
		}

			private function buildOutput():ControllerOutput
			{
				return new ControllerOutput();
			}

			protected function setupResponse():void
			{
			}

		private function tearDown():void
		{
			unset($this->input);
			unset($this->output);
			unset($this->response);
		}

	protected function getRequest():Request
	{
		return $this->input->getRequest();
	}

}
