<?php
declare(strict_types=1);

namespace slowpoke\framework\library\controller;

use slowpoke\framework\library\request\Request;

final class ControllerInput
{

	private Request $request;

	public function __construct(
		Request $request
	)
	{
		$this->request = $request;
	}

	public function getRequest():Request
	{
		return $this->request;
	}

}