<?php
declare(strict_types=1);

namespace slowpoke\framework\service\cgiAppRunner;

use slowpoke\framework\service\cgiAppRunner\CgiAppRunnerServiceItf;
use slowpoke\framework\service\cgiAppRunner\CgiAppRunnerInput;
use slowpoke\framework\service\cgiAppRunner\CgiAppRunnerOutput;
use slowpoke\framework\service\requestBuilder\RequestBuilderServiceItf;
use slowpoke\framework\service\requestBuilder\RequestBuilderInput;
use slowpoke\framework\service\app\AppServiceItf;
use slowpoke\framework\service\app\AppInput;
use slowpoke\framework\service\responseDispatcher\ResponseDispatcherServiceItf;
use slowpoke\framework\service\responseDispatcher\ResponseDispatcherInput;
use slowpoke\framework\library\request\Request;
use slowpoke\framework\library\response\Response;
use slowpoke\core\library\Error;

final class CgiAppRunnerService implements CgiAppRunnerServiceItf
{
	const ERROR_CODE_NO_REQUEST_BUILDER_REQUEST = 0;
	const ERROR_CODE_NO_APP_RESPONSE = 1;

	private RequestBuilderServiceItf $requestBuilder;

	private AppServiceItf $app;

	private ResponseDispatcherServiceItf $responseDispatcher;

	private CgiAppRunnerInput $input;

	private CgiAppRunnerOutput $output;

	private Request $request;

	private Response $response;

	public function __construct(
		RequestBuilderServiceItf $requestBuilder,
		AppServiceItf $app,
		ResponseDispatcherServiceItf $responseDispatcher
	)
	{
		$this->requestBuilder = $requestBuilder;
		$this->app = $app;
		$this->responseDispatcher = $responseDispatcher;
	}

	public function run(CgiAppRunnerInput $input):CgiAppRunnerOutput
	{
		$this->input = $input;

		$this->setupOutput();

		$output = $this->output;

		$this->tearDown();

		return $output;
	}

		private function setupOutput():void
		{
			$this->output = $this->buildOutput();

			$this->setupRequest();

			if ($this->output->hasErrors())
			{
				return;
			}

			$this->setupResponse();

			if ($this->output->hasErrors())
			{
				return;
			}

			$this->dispatchResponse();
		}

			private function buildOutput():CgiAppRunnerOutput
			{
				return new CgiAppRunnerOutput();
			}

			private function setupRequest():void
			{
				$requestBuilderOutput = $this->requestBuilder->run(
					$this->buildRequestBuilderInput()
				);

				if ($requestBuilderOutput->hasErrors())
				{
					$this->output->addErrors(
						$requestBuilderOutput->getErrorsAfterHas()
					);
					return;
				}

				if ($requestBuilderOutput->hasRequest())
				{
					$this->request = $requestBuilderOutput->getRequestAfterHas();
					return;
				}

				$this->output->addError(
					new Error(
						self::ERROR_CODE_NO_REQUEST_BUILDER_REQUEST,
						'RequestBuilder had no errors but had no request'
					)
				);
			}

				private function buildRequestBuilderInput():RequestBuilderInput
				{
					return new RequestBuilderInput();
				}

			private function setupResponse():void
			{
				$appOutput = $this->app->run(
					$this->buildAppInput()
				);

				if ($appOutput->hasErrors())
				{
					$this->output->addErrors(
						$appOutput->getErrorsAfterHas()
					);
					return;
				}

				if ($appOutput->hasResponse())
				{
					$this->response = $appOutput->getResponseAfterHas();
					return;
				}

				$this->output->addError(
					new Error(
						self::ERROR_CODE_NO_APP_RESPONSE,
						'App had no error but had no response'
					)
				);
			}

				private function buildAppInput():AppInput
				{
					return new AppInput(
						$this->request
					);
				}

			private function dispatchResponse():void
			{
				$responseDispatcherOutput = $this->responseDispatcher->run(
					$this->buildResponseDispatcherInput()
				);

				if ($responseDispatcherOutput->hasErrors())
				{
					$this->output->addErrors(
						$responseDispatcherOutput->getErrorsAfterHas()
					);
				}
			}

				private function buildResponseDispatcherInput():ResponseDispatcherInput
				{
					return new ResponseDispatcherInput(
						$this->response
					);
				}

		private function tearDown():void
		{
			unset($this->input);
			unset($this->output);
			unset($this->request);
			unset($this->response);
		}

}