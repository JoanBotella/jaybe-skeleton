<?php
declare(strict_types=1);

namespace slowpoke\framework\service\router;

use slowpoke\framework\service\router\RouterOutput;
use slowpoke\framework\service\router\RouterInput;
use slowpoke\framework\service\router\RouterServiceItf;
use slowpoke\framework\library\route\RouteItf;
use slowpoke\framework\library\requestMatcher\RequestMatcherInput;
use slowpoke\core\library\Error;

final class RouterService implements RouterServiceItf
{
	const ERROR_CODE_NO_REQUEST_MATCHER_IS_MATCH = 0;

	private array $routes;

	private RouterInput $input;

	private RouterOutput $output;

	public function __construct(
		array $routes
	)
	{
		$this->routes = $routes;
	}

	public function route(RouterInput $input):RouterOutput
	{
		$this->input = $input;

		$this->setupOutput();

		$output = $this->output;

		$this->tearDown();

		return $output;
	}

		private function setupOutput():void
		{
			$this->output = $this->buildOutput();
			$this->tryToSetupControllerToOutput();
		}

			private function buildOutput():RouterOutput
			{
				return new RouterOutput();
			}

			private function tryToSetupControllerToOutput():void
			{
				$routesLength = count($this->routes);
				for (
					$i = 0;
					(
						$i < $routesLength
						&& !$this->output->hasErrors()
						&& !$this->output->hasController()
					);
					$i++
				)
				{
					$this->tryToSetupControllerToOutputByRoute($this->routes[$i]);
				}
			}

				private function tryToSetupControllerToOutputByRoute(RouteItf $route):void
				{
					$requestMatcher = $route->getRequestMatcher();

					$requestMatcherInput = new RequestMatcherInput(
						$this->input->getRequest()
					);

					$requestMatcherOutput = $requestMatcher->match($requestMatcherInput);

					if ($requestMatcherOutput->hasErrors())
					{
						$this->output->addErrors(
							$requestMatcherOutput->getErrorsAfterHas()
						);
						return;
					}

					if ($requestMatcherOutput->hasIsMatch())
					{
						if ($requestMatcherOutput->getIsMatchAfterHas())
						{
							$this->output->setController(
								$route->getController()
							);
						}
						return;
					}

					$this->output->addError(
						new Error(
							self::ERROR_CODE_NO_REQUEST_MATCHER_IS_MATCH,
							'RequestMatcher had no errors but had no isMatch'
						)
					);
				}

		private function tearDown():void
		{
			unset($this->input);
			unset($this->output);
		}

}